package test.unitTests;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.CartiRepoInterface;
import biblioteca.repository.CartiRepoMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executor;

import static org.junit.Assert.assertTrue;



/**
 * Created by Raul on 27/03/2018.
 */
public class Lab2Tests {

    private BibliotecaCtrl controller;
    private CartiRepoInterface repository;

    @Before
    public void initialize() {
        repository = new CartiRepoMock();
        controller = new BibliotecaCtrl(repository);
    }

    @Test
    public void shortTitle() {
        Carte carte = new Carte();

        carte.setTitlu("ab");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));


        try {
            controller.adaugaCarte(carte);
            assert false;
        } catch (Exception e) {
            assertTrue(e.getMessage().equals("Titlu invalid!"));
        }
    }

    @Test
    public void emptyAuthorList() {
        Carte carte = new Carte();

        carte.setTitlu("ab");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(new ArrayList<>());


        try {
            controller.adaugaCarte(carte);
            assert false;
        } catch (Exception e) {
            assertTrue(e.getMessage().equals("Lista autori vida!"));
        }
    }

    @Test
    public void invalidAuthor() {
        Carte carte = new Carte();

        carte.setTitlu("ab");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "ab"));


        try {
            controller.adaugaCarte(carte);
            assert false;
        } catch (Exception e) {
            assertTrue(e.getMessage().equals("Autor invalid!"));
        }
    }


    @Test
    public void testInvalidAnAparitie1() {
        Carte carte = new Carte();
        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("-1");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Mihai Eminescu"));

        try {
            controller.adaugaCarte(carte);
            assert false;
        } catch (Exception e) {
            assertTrue(e.getMessage().equals("An aparitie Invalid!"));
        }
    }

    @Test
    public void testInvalidAnAparitie2() {
        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2018");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Mihai Eminescu"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    public void testInvalidAnAparitie3() {
        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2019");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Mihai Eminescu"));

        try {
            controller.adaugaCarte(carte);
            assert false;
        } catch (Exception e) {
            assertTrue(e.getMessage().equals("An aparitie Invalid!"));
        }
    }


    @Test
    public void testInvalidEditura1() {
        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("hu");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Mihai Eminescu"));

        try {
            controller.adaugaCarte(carte);
            assert false;
        } catch (Exception e) {
            assertTrue(e.getMessage().equals("Editura Invalida!"));
        }
    }

    @Test
    public void testInvalidEditura2() {
        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura(null);
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Mihai Eminescu"));

        try {
            controller.adaugaCarte(carte);
            assert false;
        } catch (Exception e) {
            assertTrue(e.getMessage().equals("Editura Invalida!"));
        }
    }


    @Test
    public void testInvalidEditura3() {
        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("hu");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Mihai Eminescu"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assertTrue(e.getMessage().equals("Editura Invalida!"));
        }
    }



    @Test
    public void testValidAdd() {
        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Mihai Eminescu"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;

        }

    }

//    public static void main(String[] args) {
//        System.out.println("aknnd");
//    }

}
