package test.unitTests;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.CartiRepoInterface;
import biblioteca.repository.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * Created by Raul on 31/03/2018.
 */
public class Lab3Tests {

    private BibliotecaCtrl controller;
    private CartiRepoInterface repository;

    @Before
    public void initialize() {
        repository = new CartiRepoMock();
        controller = new BibliotecaCtrl(repository);
    }

    @Test
    public void testSearch1() {
        controller.clear();

        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }

        carte = new Carte();

        carte.setTitlu("abcd");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }

        assertTrue(controller.cautaCarteDupaNume("ab").size() == 2);
//        System.out.println(controller.cautaCarteDupaNume("ab"));
    }


    @Test
    public void testSearch2() {
        controller.clear();


        assertTrue(controller.cautaCarteDupaNume("carte").size() == 0);
//        System.out.println(controller.cautaCarteDupaNume("ab"));
    }


    @Test
    public void testSearch3() {
        controller.clear();

        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }

        assertTrue(controller.cautaCarteDupaNume("xyz").size() == 0);
//        System.out.println(controller.cautaCarteDupaNume("ab"));
    }

    @Test
    public void testSearch4() {
        controller.clear();

        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }

        carte = new Carte();

        carte.setTitlu("xyzt");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }



        assertTrue(controller.cautaCarteDupaNume("xyz").size() == 1);
//        System.out.println(controller.cautaCarteDupaNume("ab"));
    }


}
