package test.unitTests;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.CartiRepoInterface;
import biblioteca.repository.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Raul on 25/04/2018.
 */
public class Lab4Tests {
    private BibliotecaCtrl controller;
    private CartiRepoInterface repository;

    @Before
    public void initialize() {
        repository = new CartiRepoMock();
        controller = new BibliotecaCtrl(repository);
    }

    @Test
    public void testInputValid(){
        controller.clear();

        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }

        try {
            List<Carte> cartiGasite = controller.getCartiOrdonateDinAnul("2000");
            //assertTrue(controller.cautaCarteDupaNume("ab").size() == 1);
            assertTrue(cartiGasite.get(0).getTitlu().equals("abc"));
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }


//        System.out.println(controller.cautaCarteDupaNume("ab"));

    }

    @Test
    public void testInputNonValid(){
        controller.clear();
        Carte carte = new Carte();

        carte.setTitlu("abc");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }

        try {
            controller.getCartiOrdonateDinAnul("xyz");
            assert false;
        }
        catch (Exception e) {
            assertTrue(e.getMessage().equals("Nu e numar!"));
        }
    }

}
