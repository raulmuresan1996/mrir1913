package integrationTests;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.CartiRepoInterface;
import biblioteca.repository.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Raul on 25/04/2018.
 */
public class TopDownIntegrationTest {

    private BibliotecaCtrl controller;
    private CartiRepoInterface repository;

    @Before
    public void initialize() {
        repository = new CartiRepoMock();
        controller = new BibliotecaCtrl(repository);
    }

    @Test
    public void first(){
        controller.clear();
        Carte carte = new Carte();

        carte.setTitlu("Poezii");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));
        try {
            controller.adaugaCarte(carte);
            assertTrue(controller.getCarti().size() == 1);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    public void second(){
        controller.clear();
        Carte carte = new Carte();

        carte.setTitlu("abcd");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));

        try {
            controller.adaugaCarte(carte);
        } catch (Exception e) {
            assert false;
            e.printStackTrace();
        }

        assertTrue(controller.cautaCarteDupaNume("ab").size() == 1);
        assertTrue(controller.cautaCarteDupaNume("ab").get(0).getTitlu().equals("abcd"));

    }

    @Test
    public void third(){
        controller.clear();
        Carte carte = new Carte();

        carte.setTitlu("Poezii");
        carte.setEditura("humanitas");
        carte.setCuvinteCheie(Arrays.asList("arta", "literatura"));
        carte.setAnAparitie("2000");
        carte.setReferenti(Arrays.asList("Ion Creanga", "Literatura"));
        try {
            controller.adaugaCarte(carte);
            assertTrue(controller.getCarti().size() == 1);
            assertTrue(controller.cautaCarteDupaNume("Poe").size() == 1);
            List<Carte> cartiGasite = controller.getCartiOrdonateDinAnul("2000");
            assertTrue(cartiGasite.size() == 1);
            assertTrue(cartiGasite.get(0).getTitlu().equals("Poezii"));
        } catch (Exception e) {
            assert false;
        }

    }
}
