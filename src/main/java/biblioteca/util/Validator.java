package biblioteca.util;

import biblioteca.model.Carte;

public class Validator {
	
	public static boolean isStringOK(String s) throws Exception{
		if(s == null)
			return false;
		boolean flag = s.matches("[ a-zA-Z]+");
		if(flag == false)
			throw new Exception("String invalid");
		return flag;
	}
	
	public static void validateCarte(Carte c)throws Exception{

		//Validare Cuvinte Cheie
		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}

		for(String cuvantCheie :c.getCuvinteCheie()){
			if(!isStringOK(cuvantCheie) || cuvantCheie.length() < 3 || cuvantCheie.length() > 100)
				throw new Exception("Cuvant cheie invalid!");
		}


		//Validare Autori
		if(c.getReferenti()==null || c.getReferenti().size() < 1){
			throw new Exception("Lista autori vida!");
		}

		for(String author:c.getReferenti()){
			if(!isStringOK(author) || author.length() < 3 || author.length() > 100)
				throw new Exception("Autor invalid!");
		}

		//Validare Titlu
		String titlu = c.getTitlu();
		if(!isStringOK(titlu) ||  titlu.length() < 3 || titlu.length() > 100){
			throw new Exception("Titlu invalid!");
		}



		//Validare Editura
		String editura = c.getEditura();
		if(!isStringOK(editura) ||  editura.length() < 3 || editura.length() > 100)
			throw new Exception("Editura Invalida!");



		//Validare An Aparitie
		if(!Validator.isNumber(c.getAnAparitie()))
			throw new Exception("An aparitie Invalid!");
		else{
			int anAparitie = Integer.parseInt(c.getAnAparitie());
			if(anAparitie > 2018 || anAparitie < 0)
				throw new Exception("An aparitie Invalid!");

		}
	}
	
	public static boolean isNumber(String s){
		if(s == null || s.length() >= 9)
			return false;
		return s.matches("[0-9]+");
	}
	
//	public static boolean isOKString(String s){
//		String []t = s.split(" ");
//		if(t.length==2){
//			boolean ok1 = t[0].matches("[a-zA-Z]+");
//			boolean ok2 = t[1].matches("[a-zA-Z]+");
//			if(ok1==ok2 && ok1==true){
//				return true;
//			}
//			return false;
//		}
//		return s.matches("[a-zA-Z]+");
//	}
	
}
