package biblioteca.repository;


import biblioteca.model.Carte;

import java.util.List;

public interface CartiRepoInterface {
	void adaugaCarte(Carte c);
	void modificaCarte(Carte nou, Carte vechi);
	void stergeCarte(Carte c);
	List<Carte> cautaCarte(String ref);
	List<Carte> cautaCarteDupaNume(String nume);
	List<Carte> getCarti();
	List<Carte> getCartiOrdonateDinAnul(String an);
	void clear();
}
