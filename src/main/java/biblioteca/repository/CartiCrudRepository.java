package biblioteca.repository;

import biblioteca.model.Carte;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Raul on 08/03/2018.
 */
public class CartiCrudRepository implements CartiRepoInterface {

    protected List<Carte> carti;

    public CartiCrudRepository(){
        carti = new ArrayList<Carte>();
    }

    @Override
    public void adaugaCarte(Carte c) {
        carti.add(c);
    }

    @Override
    public void modificaCarte(Carte nou, Carte vechi) {
        // TODO Auto-generated method stub

    }

    @Override
    public void stergeCarte(Carte c) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<Carte> cautaCarte(String ref) {
        List<Carte> cartiGasite = new ArrayList<>();
        int i=0;
        while (i < carti.size()){
            boolean flag = false;
            List<String> lref = carti.get(i).getReferenti();
            int j = 0;
            while(j < lref.size()){
                if(lref.get(j).toLowerCase().contains(ref.toLowerCase())){
                    flag = true;
                    break;
                }
                j++;
            }
            if(flag == true){
                cartiGasite.add(carti.get(i));
            }
            i++;
        }
        return cartiGasite;
    }

    @Override
    public List<Carte> cautaCarteDupaNume(String nume) {
        List<Carte> cartiGasite = new ArrayList<>();
        for(int i = 0; i < carti.size(); i++){
            if(carti.get(i).getTitlu().contains(nume)){
                cartiGasite.add(carti.get(i));
            }
        }
        return cartiGasite;
    }

    @Override
    public List<Carte> getCarti() {
        return carti;
    }

    @Override
    public List<Carte> getCartiOrdonateDinAnul(String an) {
        List<Carte> lc = getCarti();
        List<Carte> lca = new ArrayList<Carte>();
        for(Carte c:lc){
            if(c.getAnAparitie().equals(an)){
                lca.add(c);
            }
        }

        Collections.sort(lca,new Comparator<Carte>(){

            @Override
            public int compare(Carte a, Carte b) {
                if(a.getTitlu().compareTo(b.getTitlu())==0){
                    List<String> listaReferentiA = a.getReferenti();
                    List<String> listaReferentiB = b.getReferenti();
                    for(int i = 0; i < Math.min(listaReferentiA.size(), listaReferentiB.size()); i++){
                        if(listaReferentiA.get(i).compareTo(listaReferentiB.get(i)) != 0)
                            return listaReferentiA.get(i).compareTo(listaReferentiB.get(i));
                    }
                    return listaReferentiA.size() - listaReferentiB.size();
                }

                return a.getTitlu().compareTo(b.getTitlu());
            }

        });

        return lca;
    }

    @Override
    public void clear() {
        carti = new ArrayList<>();
    }
}
